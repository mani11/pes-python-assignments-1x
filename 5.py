
#Write a program to receive 5 command line arguments and print each argument separately.
#Example :
#             >> python test.py arg1 arg2 arg3 arg4 arg5
#  a) From the above statement your program should receive arguments and print them each of them. 
#  b) Find the biggest of three numbers, where three numbers are passed as command line arguments.

import sys
x=0
z=0
if(len(sys.argv)==6):
    # print 5 command line arguments
    for i in sys.argv:
        if(z==0):
            z+=1
            continue
        
        print(i)
        if(str.isdecimal(i)):
            if(x==0):
                result=int(i)
                x+=1
        
            elif(int(i)>result):
                result=int(i)
    print("Biggest Num is :", result)
            
else:
    print("Enter 5 Command Line arguments Please !!!")
