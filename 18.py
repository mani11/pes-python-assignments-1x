
#Using loop structures print numbers from 1 to 100.  and using the same loop print numbers from 100 to 1.( reverse printing)
     #a) By using For loop 
     #b) By using while loop
    #c) Let    mystring ="Hello world"
             #print each character of  mystring in to separate line using appropriate  loop structure.
number = 100
for i in range (1,number+1):
    print(i)

i = 0
while i < number:
    i +=1
    print (i)

mystring = "Hello world"
for i in mystring:
    print (i)

for i in range (number,0,-1):
    print (i)

i = 100
while i:
    print (i)
    i -= 1
    
for i in mystring[::-1]:
    print (i)
