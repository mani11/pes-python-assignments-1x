#Using loop structures print even numbers between 1 to 100.  
     #a) By using For loop , use continue/ break/ pass statement to skip  odd numbers.
           #i)  break the loop if the value is 50
           #ii) Use continue for the values 10,20,30,40,50

     #b) By using while loop, use continue/ break/ pass statement to skip odd numbers.
            #i)  break the loop if the value is 90
           #ii) Use continue for the values 60,70,80,90
number = 100
for i in range (1,number):
    if i%2 == 0:
        if i == 50:
            break
        elif i in range(10,51,10):
            continue
        print (i)

    

i = 1
while i <= 100:
    if i%2 == 0:
        if i == 90:
            break
        elif i in range (60,91,10):
            continue
        print (i)
    i += 1

