#Using assignment operators, perform following operations
     #Addition, Substation, Multiplication, Division, Modulus, Exponent and Floor division operations

a = 70
b = 15
c = 0

c = a + b
print ("Line 1 - Value of c is ", c)

c += a
print ("Line 2 - Value of c is ", c) 

c *= a
print ("Line 3 - Value of c is ", c)

c /= a 
print ("Line 4 - Value of c is ", c) 

c %= a
print ("Line 5 - Value of c is ", c)

c **= a
print ("Line 6 - Value of c is ", c)

c //= a
print ("Line 7 - Value of c is ", c)
