#Write a program to create two list A and B such that List A contains Employee Id, List B contains Employee name
    #( minimum 10 entries in each list ) and perform following operations
    #a) Print all names on to screen
    #b) Read the index from the  user and print the corresponding name from both list.
    #c) Print the names from 4th position to 9th position
    #d) Print all names from 3rd position till end of the list
    #e) Repeat list elements by specified number of times ( N- times, where N is entered by user)
    #f)  Concatenate two lists and print the output.
    #g) Print element of list A and B side by side.( i.e.  List-A First element ,  List-B First element )
list1 = int(input("no. of id"))
list2 = int(input("no of name"))
Employee_id = []
Employee_name = []
for i in range(list1):
    num = int(input("enter the ids:"))
    Employee_id.append(num)
print (Employee_id)

for j in range(list2):
    names = input ("enter the name:")
    Employee_name.append(names)
print (Employee_name)
index = int(input("enter the index"))
print (Employee_name[index])
print (Employee_name[4:10])
print (Employee_name[3:])
repeat = int(input("enter the no. of time you want the repeation:"))
print (Employee_id*repeat)
print (Employee_name*repeat)
print (Employee_id+Employee_name)#concatenate with +
print ("{} {}".format(Employee_id,Employee_name))#concatenate with Format
for i , j in zip(Employee_id,Employee_name):
    print (i,j)
